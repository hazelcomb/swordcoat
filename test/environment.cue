package test

import (
    "dagger.io/dagger"
    "dagger.io/dagger/core"
    "universe.dagger.io/docker"
    "universe.dagger.io/bash"
)

#Environment: {
    src: dagger.#FS

    variables: {
        BUILDKIT_HOST: "docker-container://dagger-buildkitd-ci"
        DAGGER_LOG_FORMAT: "plain"
        PATH: "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/src/bin"
    }

    packages: {
        curl: {}
        bash: {}
        docker: {}
        yarn: {}
        git: {}
    }

    image: docker.#Build & {
        steps: [
            docker.#Pull & {
                source: "docker:dind"
            },
            for pkgName, pkg in packages {
                docker.#Run & {
                    command: {
                        name: "apk"
                        args: ["add", "\(pkgName)"]
                        flags: {
                            "-U":         true
                            "--no-cache": true
                        }
                    }
                }
            }
        ]
    }

    node: bash.#Run & {
        workdir: "src"
        input: image.output
        script: contents: "yarn add bats bats-support bats-assert"
        export: directories: ".": dagger.#FS
    }

    daggerBinary: bash.#Run & {
        workdir: "src"
        env: variables
        input: image.output
        script: contents: """
            curl -L https://dl.dagger.io/dagger/install.sh | sh
            dagger project init
            dagger project update
        """
        export: {
            directories: ".": dagger.#FS
        }
    }

    repository: {
        source: core.#Mkdir & {
            input: src
            path: "cue.mod/pkg/gitlab.com/hazelcomb/swordcoat"
        }

        copy: core.#Copy & {
            include: [
                "common",
                "node",
                "nginx"
            ]
            input: src
            contents: source.output
            dest: "cue.mod/pkg/gitlab.com/hazelcomb/swordcoat"
        }

        output: copy.output
    }

    source: core.#Merge & {
            inputs: [src, node.export.directories.".", daggerBinary.export.directories.".", repository.output]
    }
}
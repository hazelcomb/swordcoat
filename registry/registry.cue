package registry

import (
    "universe.dagger.io/docker"
    "gitlab.com/hazelcomb/swordcoat/types"
)

#Push: {
    input: docker.#Image
    configuration: types.#Config
    
    _push: docker.#Push & {
        image: input
        dest: configuration.tag
        if configuration.password != _|_ {
            auth: {
                username: configuration.username | *"regadmin"
                secret: configuration.password
            }
        }
    }

    output: _push.result
}

#Pull: {
    configuration: types.#Config

    docker.#Pull & {
        source: configuration.tag
        if configuration.password != _|_ {
            auth: {
                username: configuration.username | *"regadmin"
                secret: configuration.password
            }
        }
    }
}

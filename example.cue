package main

import (
    "dagger.io/dagger"

    "gitlab.com/hazelcomb/swordcoat/configuration"
    "gitlab.com/hazelcomb/swordcoat/node"
    "gitlab.com/hazelcomb/swordcoat/registry"
    "gitlab.com/hazelcomb/swordcoat/compose"
    "gitlab.com/hazelcomb/swordcoat/nginx"
)

dagger.#Plan & {
    client: {
        env: {
			REGISTRY_PW?: dagger.#Secret
			CONFIG_FILE: string | *"swordcoat.yml"
		}
        filesystem: {
            project: read: {
                path: "."
                contents: dagger.#FS
                exclude: ["node_modules", "README.md"]
            }
            dist: write: {
                path: "dist"
                contents: actions.build.output
            }
        }
        network: {
            docker: {
                address: actions.config.output.docker.socket
                connect: dagger.#Socket
            }
        }
    }

    _src: client.filesystem.project.read.contents

    actions: {
        config: configuration.#Read & {
            src: _src
            file: client.env.CONFIG_FILE
            registry: password: client.env.REGISTRY_PW | *null
        }

        build: node.#Build & {
            src: _src
        }

        dockerize: nginx.#Dockerize & {
            src: _src
            configuration: config.output
        }

        push: registry.#Push & {
            input: dockerize.output
            configuration: config.output
        }

        run: compose.#Run & {
            image: dockerize.output
            socket: client.network.docker.connect
            configuration: config.output
        }

        delete: compose.#Delete & {
            socket: client.network.docker.connect
            configuration: config.output
        }
    }
}
package common

import (
    "universe.dagger.io/alpine"
    "universe.dagger.io/bash"
)

#Echo: {
    text: string

    container: bash.#Run & {
        input:  *_image.output | _
        _image: alpine.#Build & {
            packages: {
                bash: {}
            }
        }
        args: ["Text:", text]
        script: contents: """
            echo $@
        """
    }
}

#Env: {
    container: bash.#Run & {
        input:  *_image.output | _
        _image: alpine.#Build & {
            packages: {
                bash: {}
            }
        }
        script: contents: """
            set
        """
    }
}
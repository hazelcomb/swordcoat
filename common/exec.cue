package common

import(
    "dagger.io/dagger"
    "universe.dagger.io/alpine"
    "universe.dagger.io/bash"
)


#Exec: {
    filesystem: dagger.#FS 
    bash.#Run & {
        input:  *_image.output | _
        _image: alpine.#Build & {
            packages: {
                bash: {}
            }
        }
        mounts: Source: {
            dest: "/src"
            contents: filesystem
        }
    }
}
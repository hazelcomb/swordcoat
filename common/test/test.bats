setup() {
    load '/src/test/bats'
    init
}

@test "exec" {
    dagger "do" -p ./exec.cue test
}

@test "fs" {
    dagger "do" -p ./fs.cue test
}
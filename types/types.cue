package types

import(
    "encoding/yaml"
    
    "dagger.io/dagger"
    "dagger.io/dagger/core"
)

#Wildcard: string | number | bool | null | core.#Secret

#Config: {
    project: string
    tag: string
    username?: string
    password: dagger.#Secret
    registry?: {
        url: string
    }
    docker: {
        socket: string
        publish?: [...string]
    }
}

#Compose: {
    configuration: #Config
    output: yaml.Marshal({
        version: "3.9"
        services: {
            frontend: {
                ports: configuration.docker.publish
                image: configuration.tag
            }
        }
    })
}
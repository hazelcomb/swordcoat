package common

import (
    "dagger.io/dagger"
    "dagger.io/dagger/core"
    "gitlab.com/hazelcomb/swordcoat/common"
)

dagger.#Plan & {
    actions: {
        test: {
            exec: {
                init: {
                    source: core.#Source & {
                        path: "."
                    }
                }

                run: common.#Exec & {
                    filesystem: init.source.output
                    script: contents: "echo -n 'executed' > /output.txt"
                }

                verify: core.#ReadFile & {
                    input: run.output.rootfs
                    path:  "/output.txt"
                } & {
                    contents: "executed"
                }
            }
        }
    }
}
# configuration attributes

| name     | description                                        | type                        | optional |
|----------|----------------------------------------------------|-----------------------------|----------|
| project  | Name of the project used for docker image name     | string                      | ❌      |
| registry | attributes of the docker registry                  | [complex object](#registry) | ✔️      |
| docker   | attributes releated to the docker engine           | [complex object](#docker)   | ❌      |

## registry

| name     | description                                        | type                        | optional |
|----------|----------------------------------------------------|-----------------------------|----------|
| url      | URL where to access the docker registry            | string                      | ❌      |

## docker

| name     | description                                        | type                        | optional |
|----------|----------------------------------------------------|-----------------------------|----------|
| socket   | path of the docker socket from the host system     | string                      | ❌      |
| publish  | array of ports which will be publish in `run` task | string array                | ✔️      |

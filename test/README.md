# integration test

Integration test of dagger jobs are done by using [bats](https://github.com/bats-core/bats-core). Bats is a automation testing system to execute tests in a [bash](https://en.wikipedia.org/wiki/Bash_(Unix_shell)).

> ⚠️currently very complex environment and may need to be optimized. Bats is started by a dagger job and bats executes all dagger plans of integration tests. This should be changed to directly starting bats and get rid of dagger inside dagger.

## example

A integration test consists of a bash script and a dagger plan. The bash script can contain multiple executions for different dagger plans. In general every dagger plan of a integration test contains three tasks to prepare some preconditions, execute a dagger task and verify the outcome of the dagger task.

### bats execution script
```sh
setup() {
    load '/src/test/bats'
    init
}

@test "exec" {
    dagger "do" -p ./exec.cue test
}

@test "fs" {
    dagger "do" -p ./fs.cue test
}
```

### dagger plan of the integration test
```cue
package common

import (
    "dagger.io/dagger"
    "dagger.io/dagger/core"
    "gitlab.com/hazelcomb/swordcoat/common"
)

dagger.#Plan & {
    actions: {
        test: {
            exec: {
                init: {
                    source: core.#Source & {
                        path: "."
                    }
                }

                run: common.#Exec & {
                    filesystem: init.source.output
                    script: contents: "echo -n 'executed' > /output.txt"
                }

                verify: core.#ReadFile & {
                    input: run.output.rootfs
                    path:  "/output.txt"
                } & {
                    contents: "executed"
                }
            }
        }
    }
}
```

# ci environment

- shell runner
- installed dagger binary
- start buildkit docker for internal use

```sh
docker volume create dagger-buildkitd-ci
docker run --restart unless-stopped --name dagger-buildkitd-ci -v dagger-buildkitd-ci:/var/lib/buildkit -d --net=host --privileged moby/buildkit "--debug"
```
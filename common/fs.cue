package common

import (
    "encoding/yaml"

    "dagger.io/dagger"
    "dagger.io/dagger/core"
)

#ParseYAML : {
    filesystem: dagger.#FS
    file: string | *"swordcoat.yml"

    _readConfig: core.#ReadFile & {
        input: filesystem
        path: file
    }

    output: yaml.Unmarshal(_readConfig.contents)
}
package compose

import (
    "dagger.io/dagger"
    "dagger.io/dagger/core"

    "universe.dagger.io/docker"
    "universe.dagger.io/docker/cli"

    "gitlab.com/hazelcomb/swordcoat/types"
)

#Build: {
    configuration: types.#Config

    _build: types.#Compose & {
        "configuration": configuration
    }

    _source: core.#Source & {
        path: "."
    }

    _write: core.#WriteFile & {
        input: _source.output
        path: "compose.yaml"
        contents: _build.output
    }

    output: _write.output
}

#Exec: {
    compose: dagger.#FS
    args: [...string]

    _docker: docker.#Pull & {
        source: "docker:20.10.19-dind-alpine3.16"
    }

    cli.#Run & {
        input: _docker.output
        workdir: "compose"
        mounts: "compose": {
            {
                dest:     "."
                contents: compose
            }
        }
        command: {
            name: "docker"
            "args": ["compose"] + args
        }
    }
}

#Run: {
    image: docker.#Image
    configuration: types.#Config
    socket: core.#Socket

    _compose: #Build & {
        "configuration": configuration
    }
    
    _load: cli.#Load & {
        "image": image
        host: socket
        tag: "\(configuration.tag)"
    }

    #Exec & {
        host: socket
        compose: _compose.output
        always: true
        args: ["up", "-d"]
        env: HACK: "\(_load.success)" // <== HACK: CHAINING of action happening here
    }
}

#Delete: {
    configuration: types.#Config
    socket: core.#Socket

    _compose: #Build & {
        "configuration": configuration
    }

    #Exec & {
        host: socket
        compose: _compose.output
        always: true
        args: ["rm", "-fsv"]
    }
}

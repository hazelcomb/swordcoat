# docker registry

The docker registry is used to publish docker images and can also be used as cache server. The docker registry has a lot of different storage solutions in this example a s3 storage from min.io is used.

1. Pull registry docker: ``docker pull registry``
0. Start registry docker: ``docker run -d -p 5000:5000 --restart always --name registry -v D:/develop/docker/registry/config.yml:/etc/docker/registry/config.yml -v D:/develop/docker/registry/auth:/auth/ registry``

```yml
version: 0.1
log:
  fields:
    service: registry
storage:
  cache:
    blobdescriptor: inmemory
  s3:
    accesskey: accesskey
    secretkey: secretkey
    region: de-east
    regionendpoint: https://s3.hazelcomb.tech
    bucket: docker
    secure: true
auth:
  htpasswd:
    realm: basic-realm
    path: /auth/htpasswd
```
config reference: https://docs.docker.com/registry/configuration/

> cause of some issues with the nginx proxy in front of the s3 storage it was needed to use the local address.

## list images

``https://registry.hazelcomb.tech/v2/_catalog``

## inspect image

``https://registry.hazelcomb.tech/v2/dagger-example/tags/list``
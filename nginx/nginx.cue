package nginx

import (
    "dagger.io/dagger"
    "dagger.io/dagger/core"

    "universe.dagger.io/docker"

    "gitlab.com/hazelcomb/swordcoat/types"
)


#Dockerize: {
    src: dagger.#FS
    configuration: types.#Config

    _pull: docker.#Pull & {
        source: "index.docker.io/nginx:stable-alpine"
    }

    _source: core.#Mkdir & {
        input: src
        path: "dist/\(configuration.project)"
    }

    docker.#Copy & {
        input: _pull.output
        contents: _source.output
        source: "dist/\(configuration.project)"
        dest: "/usr/share/nginx/html"
    }
}
# table of content

- [dagger example](#dagger-example)
- [buildkit](#buildkit)
- [registry](opt/registry/README.md)
- [configuration](configuration/README.md)
- [integration tests](test/README.md)
- [Links](#links)

# dagger example

This is an example angular application to show how to use dagger as gateway between CI/CD environment and docker images. Dagger improves **reusability** of jobs and **simplifies** configuration of the CI/CD environment. The biggest benefit for me is to execute CI/CD jobs **locally**, therefore you can test them before pushing into the repository.

<div align="center">
  <img src="opt/docs/environment.drawio.png" alt="environment" title="environment"/>
</div>

## project initialization

1. create angular project: `ng new dagger-example`
0. change directory: `cd dagger-example`
0. output readable logs: `export DAGGER_LOG_FORMAT=plain`
0. create dagger project: `dagger project init`
0. download shared dagger projects: `dagger project update`
0. download and add custom project: `dagger project update gitlab.com/hazelcomb/swordcoat@main`
0. copy example dagger plan: `cp cue.mod/pkg/gitlab.com/hazelcomb/swordcoat/example.cue dagger.cue`
0. copy example dagger plan: `cp cue.mod/pkg/gitlab.com/hazelcomb/swordcoat/swordcoat.yml swordcoat.yml`
0. execute angular build action: `dagger do build`
0. OPTIONAL: execute push to registry: `REGISTRY_PW=ch4ng317 && dagger do push`
0. starts a docker container on host system: `dagger do run`

> The environment variable `DAGGER_LOG_FORMAT=plain` is needed for git-bash on a windows machine otherwise dagger will use json output.

> The custom dagger project swordcoat will be added to the files `dagger.mod` and `dagger.sum`. Afterward you just need to run `dagger project update` to update it to the latest version.

> The project swordcoat is a public repository to use private repositories you need to specify options `--private-key-file` and `--private-key-password`.

> I really recommend the vscode extentions in this repository. It will help a lot. 😉


### youtube preview

<div align="center">
  <a href="https://youtu.be/qVMExz4-UDs"><img src="https://img.youtube.com/vi/qVMExz4-UDs/0.jpg" alt="swordcoat - dagger.io job repository" title="swordcoat - dagger.io job repository"/></a>
</div>

# buildkit

> ⚠️dangerous superficial knowledge

Dagger is using [Buildkit](https://github.com/moby/buildkit) to build docker images, runing docker images, caching data exporting data from docker runtime. Buildkit is a [DAG](https://en.wikipedia.org/wiki/Directed_acyclic_graph) execution engine. In general Buildkit is execute with [golang](https://go.dev/) and dagger simplifies this by using [cuelang](https://cuelang.org/) as configuration.

# Links

- [What is CUE?](https://docs.dagger.io/1215/what-is-cue/)
- [built-in classes in CUE](https://github.com/cue-lang/cue/tree/master/pkg)
- [built-in schemas in dagger](https://github.com/dagger/dagger/tree/main/pkg)

package common

import (
    "dagger.io/dagger"
    "dagger.io/dagger/core"
    "gitlab.com/hazelcomb/swordcoat/common"
)

dagger.#Plan & {
    actions: {
        test: {
            exec: {
                init: {
                    source: core.#Source & {
                        path: "."
                    }

                    defaultYaml: core.#WriteFile & {
                        input: source.output
                        path: "swordcoat.yml"
                        contents: "project: test-project"
                    }

                    customYaml: core.#WriteFile & {
                        input: source.output
                        path: "custom.yml"
                        contents: "project: custom-project"
                    }
                }

                run: {
                    default: common.#ParseYAML & {
                        filesystem: init.defaultYaml.output
                    }

                    custom: common.#ParseYAML & {
                        filesystem: init.customYaml.output
                        file: "custom.yml"
                    }
                }

                verify: {
                    default: run.default.output & {
                        project: "test-project"
                    }

                    custom: run.custom.output & {
                        project: "custom-project"
                    }
                }
            }
        }
    }
}
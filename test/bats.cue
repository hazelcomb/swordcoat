package test

import (
    "dagger.io/dagger"
    "dagger.io/dagger/core"
    "universe.dagger.io/bash"
)

#Run: {
    socket: core.#Socket
    src: dagger.#FS
    variables: [string]: string

    testSuites: "$(find -type f -name '*.bats' -not -path '*/node_modules/*' -not -path '*/cue.mod/*')"

    bash.#Run & {
        workdir: "src"
        env: variables
        mounts: {
            modules: {
                dest:     "/src"
                contents: src
            }
            docker: {
                dest:     "/var/run/docker.sock"
                contents: socket
            }
        }
        script: contents: "yarn bats --print-output-on-failure --verbose-run \(testSuites) ."
        // script: contents: "ls -la cue.mod/pkg/gitlab.com/hazelcomb/swordcoat"
    }
}
package node

import (
    "dagger.io/dagger"
    "dagger.io/dagger/core"
    "universe.dagger.io/alpine"
    "universe.dagger.io/bash"
)	

_container: {
    project: string
    filesystem: dagger.#FS
    out: [path=string]: dagger.#FS

    bash.#Run & {
        input:  *_image.output | _
        _image: alpine.#Build & {
            packages: {
                bash: {}
                yarn: {}
                git: {}
            }
        }

        workdir: "/src"
        mounts: Source: {
            dest:     "/src"
            contents: filesystem
        }
        export: directories: out

        // Setup caching
        env: YARN_CACHE_FOLDER:  "/cache/yarn"
        mounts: {
            "Yarn cache": {
                dest:     "/cache/yarn"
                contents: core.#CacheDir & {
                    id: "\(project)-yarn"
                }
            }
            "NodeJS cache": {
                dest:     "/src/node_modules"
                type:     "cache"
                contents: core.#CacheDir & {
                    id: "\(project)-nodejs"
                }
            }
        }
    }
}

#Build: {
    src: dagger.#FS
    project: string | *"default"
    versionFile: string | *"src/environments/version.ts"
    

    // install dependencies
    install: {
        container: _container & {
            filesystem: src
            "project": project
            script: contents: "yarn install --frozen-lockfile"
            out: ".": dagger.#FS
        }
        output: container.export.directories."."
    }

    // Create version file with git revision
    version: {
        container: _container & {
            filesystem: src
            "project": project
            args: [versionFile]
            script: contents: """
                REVISION=$(git describe --always)
                echo revision=$REVISION
                JSON=$(printf \"export const version = { revision: '%s' };\" $REVISION)
                echo \"$JSON\" > $1
            """
            out: ".": dagger.#FS
        }
        _merge: core.#Merge & {
            inputs: [src, install.output, container.export.directories."."]
        }
        output: _merge.output
    }
    
    // execute build
    run: {
        container: _container & {
            filesystem:  version.output
            "project": project
            // working dir where the build output is generated is a mounted devices,
            // thats why it is needed to move the builded files outside to export it from this task
            script: contents: """
                yarn run build
                mv dist /
            """
            out: "/dist": dagger.#FS
        }
        output: container.export.directories."/dist"
    }

    output: run.output
}
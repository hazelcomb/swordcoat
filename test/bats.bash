init() {
    load "/src/node_modules/bats-support/load.bash"
    load "/src/node_modules/bats-assert/load.bash"

    # cd into the directory containing the bats file
    cd "$BATS_TEST_DIRNAME" || exit 1
}
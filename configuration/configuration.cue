package configuration

import(
    "dagger.io/dagger"
    "gitlab.com/hazelcomb/swordcoat/types"
    "gitlab.com/hazelcomb/swordcoat/common"
)

#Read: {
    src: dagger.#FS
    always: true
    file: string | null
    registry: password: dagger.#Secret

    _config: common.#ParseYAML & {
        filesystem: src
        "file": file
    }

    output: types.#Config & _config.output & {
        tag: "\(_config.output.registry.url)/\(_config.output.project)"
        password: registry.password
    }
}
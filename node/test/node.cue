package common

import (
    "dagger.io/dagger"
    "dagger.io/dagger/core"
    "universe.dagger.io/bash"
    "universe.dagger.io/alpine"
    "gitlab.com/hazelcomb/swordcoat/node"
)

dagger.#Plan & {
    client: filesystem: ".": read: {
        include: [
            "package.json",
            "tsconfig.json",
            "index.ts"
        ]
    }

    actions: {
        test: {
            init: {
                image: alpine.#Build & {
                    packages: {
                        bash: {}
                        yarn: {}
                        git: {}
                    }
                }

                git: bash.#Run & {
                    workdir: "/src"
                    input: image.output
                    script: contents: """
                        git config --global init.defaultBranch main
                        git init
                        git config --global user.email 'you@example.com'
                        git config --global user.name 'Your Name'
                        git commit --allow-empty -m 'initial commit'
                        git describe --always > hash.txt
                    """
                    export: directories: ".": dagger.#FS
                }

                source: core.#Merge & {
                    inputs: [client.filesystem.".".read.contents, git.export.directories."."]
                }

                output: source.output
            }

            exec: {
                build: node.#Build & {
                    src: init.output
                    versionFile: "version.ts"
                }

                run: bash.#Run & {
                    input: init.image.output
                    workdir: "/dist"
                    mounts: dist: {
                        dest:     "/dist"
                        contents: build.output
                    }
                    script: contents: "node index.js > /output.txt"
                }

                verify: {
                    core.#ReadFile & {
                        input: run.output.rootfs
                        path:  "/output.txt"
                    } & {
                        contents: "Hello World!\n"
                    }
                }
            }
        }
    }
}